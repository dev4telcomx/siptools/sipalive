#
# Dev4Telco | https://dev4telco.mx | ping@dev4telco.mx
# https://www.youtube.com/@dev4telco516 | https://twitter.com/dev4telco
#
from decouple import config, Config, RepositoryEnv
env_config  = Config(RepositoryEnv('/usr/local/sipalive/.env'))
ROL         = config("ROL")
IFACE       = config("IFACE")
MASK        = config("MASK")
IP          = config("IP")
IP_PORT     = int(config("IP_PORT"))
COUNT       = int(config("COUNT"))
INTERVAL    = int(config("INTERVAL"))
TIMEOUT     = int(config("TIMEOUT"))
FAILURES    = int(config("FAILURES"))
