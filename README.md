# sipalive

[sipalive](https://gitlab.com/dev4telcomx/siptools/sipalive.git) is a python daemon/service that allows you to run SIP VoIP cluster with HA (high availability) thats supports different voice core solutions like Asterisk/FreeSWITCH/Opensips/Kamailio or anything that responds to sip options.

For now it only runs on Debian, and was already tested on version 12.1 bookworm

This service is based on Laurie Odgers work, [sipping](https://github.com/laurieodgers/sipping)
 who holds all credits for this library.

this service would run on each voip node, sharing the same floating ip and port where the SIP server runs.

#### sipalive use the following rules to switch between nodes

| Floating Interface<br>(local) | Floating IP<br>(ping) | SIP ping<br>(options)  | Action | Description |
| :------: | :------: | :------: | :---- |:---- |
| up   | up   | up   | -                           | Healthy active/master server |
| down | up   | up   | -                           | Healthy active/master server |
| up   | up   | down | bring down floating ip      | When active server is down |
| down | up   | down | bring up floating interface | When passive server detects SIP server is down |
| down | down | dewn | bring up floating interface | When passive server detects floating IP is down |


## Videos

### [sipalive POC](https://youtu.be/qhFcUBbg9I8) (probe of concept) in youtube (spanish) 
### [tutorial](https://youtu.be/tpKCNOgYZrU) step by step installation (spanish)

## 1. Installation
Download sipalive from repo
```bash
cd /usr/local/
git clone https://gitlab.com/dev4telcomx/siptools/sipalive.git
cd sipalive/
```

the installation script will install all dependencies and configure the service in the systemctl daemon, please run it on both nodes (master/active & slave/passive)
```bash
./install
```


## 2. Configuring master/active
Use env demo file to configure master node
```bash
cp .env_master to .env
``````
Edit .env file and put your cluster floating interface, ip & port on it
```bash
ROL="master"
IFACE="eth3:1"
MASK="24"
IP="192.168.1.10"
IP_PORT=5060
COUNT=1
INTERVAL=1
TIMEOUT=3
FAILURES=5
```

## 3. Configuring slave/passive
Use env demo fole to configure slave node
```bash
cp .env_slave to .env
``````
Edit .env file and put your cluster floating interface, ip & port on it
```bash
ROL="slave"
IFACE="eth3:1"
MASK="24"
IP="192.168.1.10"
IP_PORT=5060
COUNT=1
INTERVAL=1
TIMEOUT=3
FAILURES=5
```

## 4. Starting the cluster
To start the cluster, run first the sipalive service on master node
```bash
systemctl start sipalive
```
when master be up, start the sipalive service on slave node


## 5. Monitoring:
you can find sipalive logs on the following file to check the node status:
```bash
/usr/local/sipalive/logs/${hostname}_node.log
```

## License
sipalive 2023 is licenced under Attribution-ShareAlike 4.0 International.
To view a copy of this license, [visit](http://creativecommons.org/licenses/by-sa/4.0/).

happy switchovers
