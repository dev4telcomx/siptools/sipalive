#!/usr/bin/python3
#
# Dev4Telco | https://dev4telco.mx | ping@dev4telco.mx
# https://www.youtube.com/@dev4telco516 | https://twitter.com/dev4telco
#
import sys
import os
import time
import signal
import socket
import logging as log
import subprocess

from lib.sipping import SipPing
from lib import config as cnf

log.basicConfig(format='%(asctime)s - %(message)s',level=log.DEBUG)

myrol               = cnf.ROL
floating_iface      = cnf.IFACE
floating_mask       = cnf.MASK
floating_ip         = cnf.IP
floating_ip_port    = cnf.IP_PORT
count               = cnf.COUNT
interval            = cnf.INTERVAL
timeout             = cnf.TIMEOUT
failures            = cnf.FAILURES

def check_iface_status(iface,ip_addr):
    cmd     = f"ifconfig {iface}|grep {ip_addr}"
    log.debug(f"[iface] [check] {cmd}")
    ps      = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    if( ps.communicate()[0] ):
        return True
    return False

def ping_iface(ip_addr,ip_port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM | socket.SOCK_NONBLOCK)
    try:
      s.connect((ip_addr, int(ip_port)))
      s.shutdown(2)
      return True
    except:
      return False

def claim_floating_ip(iface,ip_addr,ip_mask):
    cmd = f"ifconfig {iface} {ip_addr}/{ip_mask} up"
    log.debug(f"[iface] [bring_up] {cmd}")
    os.system(cmd)
    return "master"

def takedown_iface(iface):
    cmd = f"ifconfig {iface} down"
    log.debug(f"[iface] [bring_down] {cmd}")
    os.system(cmd)
    return "slave"

def handle_sigint(sig, frame):
    show_stats()
    sys.exit(0)

def show_stats():
    end_time = time.time()
    num_received = 0
    num_timed_out = 0
    total_latency = 0
    min_latency = (timeout * 1000) + 1
    max_latency = 0
    for result in results:
        if (result is None):
            num_timed_out = num_timed_out + 1
        else:
            num_received = num_received + 1
            total_latency = total_latency + result

            if (result < min_latency):
                min_latency = result

            if (result > max_latency):
                max_latency = result

    packetloss_percentage = (num_timed_out / len(results)) * 100

    if (num_received > 0):
        # calculate the avg latency
        avg_latency = total_latency / num_received
    time_result = round((end_time - start_time) * 1000, 3)

    myrol = takedown_iface(floating_iface)
    log.info(f"[stats] --- {floating_ip}:{floating_ip_port} ping statistics ---")
    log.info(f"[stats] {len(results)} packets transmitted, {num_received} packets received, {packetloss_percentage}% packet loss")

    if (num_received > 0):
        log.debug(f"[stats] round-trip min/avg/max = {round(min_latency*1000,3)}/{round(avg_latency*1000,3)}/{round(max_latency*1000,3)} ms")
    log.info(f"[service] sipalive stopped!")

# MAIN
results     = []
fails       = 0
start_time  = time.time()
sipping     = SipPing(floating_ip, floating_ip_port)
if myrol == "slave": failures+=1

log.info(f"[service] sipalive started!")
signal.signal(signal.SIGINT, handle_sigint)     # catch ctrl-c
signal.signal(signal.SIGTERM, handle_sigint)    # catch systemctl stop

while (True):
    if(fails >= failures):
        log.info(f"[options] max failures:{fails}/{failures} reached!")

        iface_status    = ping_iface(floating_ip,floating_ip_port)
        iface_is_here   = check_iface_status(floating_iface,floating_ip)

        if( iface_is_here == True and iface_status == True):
            log.info(f"[iface] taking down {floating_iface} iface is running here?[{iface_is_here}] iface is up?[{iface_status}]")
            myrol = takedown_iface(floating_iface)
            time.sleep(timeout)
        elif( iface_is_here == True and iface_status == False):
            log.info(f"[iface] taking down! {floating_iface} iface is running here?[{iface_is_here}] iface is up?[{iface_status}]")
            myrol = takedown_iface(floating_iface)
            time.sleep(timeout)
        elif( iface_is_here == False and iface_status == False):
            log.info(f"[iface] claiming!  iface is here?[{iface_is_here}] iface status?[{iface_status}]")
            if(myrol == "slave"):
                time.sleep(2)
            myrol = claim_floating_ip(floating_iface,floating_ip,floating_mask)
        else:
            log.debug(f"if you see this message something went wrong! iface is here? {iface_is_here} | iface status? {iface_status} |")
        fails = 0
    result = None
    try:
        result = sipping.ping_once()
        results.append(result)
    except:
        pass
    if (result is None):
        log.info(f"[options] Request timed out to {floating_ip}:{floating_ip_port}")
        fails+=1
    else:
        log.debug(f"[options] [{myrol}] Reply from {floating_ip}:{floating_ip_port} time= {round(result*1000, 3)} ms")
        fails=0
    time.sleep(interval)
